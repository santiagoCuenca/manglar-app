import 'package:flutter/material.dart';
import 'dart:io';

class DisplayPicture extends StatelessWidget {

  final String imagePath;
  final String title;

  const DisplayPicture({Key key, this.imagePath, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Center(
        child: Image.file(File(imagePath)),
      ),
    );
  }

}
import 'style.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import '../constants.dart';
import '../plugins/multi_select_dialog_item.dart';

class Utility {

  static Future<dynamic> loadConfig(String path) async {
    String jsonString = await rootBundle.loadString(path);
    return json.decode(jsonString);
  }

  static Future<String> getLocalStorage(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<Null> setLocalStorage(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  static Future<Set<int>> getValueMultipleSelect(BuildContext context, List<String> options, String title, Set<int> selected ) async {

    final items = options.map<MultiSelectDialogItem<int>>((String option){
      var index = options.indexOf(option) + 1;
      return MultiSelectDialogItem(index, option);
    }).toList();

    final selectedValues = await showDialog<Set<int>>(
      context: context,
      builder: (BuildContext context) {
        return MultiSelectDialog(
          title: title,
          items: items,
          initialSelectedValues: selected,
        );
      },
    );
    return selectedValues;
  }

  static navTo(context, Widget w){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => w),
    );
  }

  static navBack(context){
    Navigator.pop(context);
  }

  static showConfirm(context, String title, String content, Function onConfirm){
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(title, style: StyleApp.getStyleTitle(18),),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
                onConfirm();
              },
            ),
          ],
        );
      },
    );
  }

  static showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1
    );
  }

  static showToastMoreTime(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1
    );
  }

  static bool _isShowingLoading = false;

  static showLoading(BuildContext context, {msg: 'Cargando..'}){
    if ( _isShowingLoading ) return;
    if ( MaterialLocalizations.of(context) == null ) {
      showToastMoreTime(msg);
      return;
    }
    _isShowingLoading = true;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            width: 140,
            height: 138,
            padding: EdgeInsets.all(24),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(Constants.colorPrimary)),
                ),
                SizedBox(width: 20, height: 20,),
                Text(msg, style: TextStyle(fontSize: 14),),
              ],
            ),
          ),
        );
      },
    );
  }

  static dismissLoading(BuildContext context){
    if ( !_isShowingLoading ) return;
    _isShowingLoading = false;
    Navigator.pop(context);
  }

  static fixName(String name){
    if ( name == null) return name;
    return name.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
  }

}

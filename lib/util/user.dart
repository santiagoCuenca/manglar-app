import '../constants.dart';
import 'utility.dart';
import '../plugins/internet_connection.dart';

class User {

  static String role = 'public'; // By default
  static String userId;
  static String userName;
  static String pass;
  static String userPin;
  static String userEmail;
  static bool hasInternetConnection = false;

  static Function onUserChange;

  static setOnUserChange(Function onChange){
    onUserChange = onChange;
  }

  static Future<Null> setInfoFromLocalStorage() async {
    role = await Utility.getLocalStorage('role') ?? 'public';
    userName = await Utility.getLocalStorage('userName');
    pass = await Utility.getLocalStorage('pass');
    userPin = await Utility.getLocalStorage('userPin');
    userId = await Utility.getLocalStorage('userId');
    userEmail = await Utility.getLocalStorage('userEmail');
  }

  static Future<Null> login(role, userId, userName, pass, userPin, userEmail) async {
    await Utility.setLocalStorage('role', role);
    await Utility.setLocalStorage('userName', userName);
    await Utility.setLocalStorage('pass', pass);
    await Utility.setLocalStorage('userPin', userPin);
    await Utility.setLocalStorage('userId', userId);
    await Utility.setLocalStorage('userEmail', userEmail);
    User.role = role;
    User.userName = userName;
    User.pass = pass;
    User.userPin = userPin;
    User.userId = userId;
    User.userEmail = userEmail;
    if (onUserChange != null) onUserChange();
  }

  static Future<Null> logout() async {
    role = 'public';
    userName = null;
    pass = null;
    userPin = null;
    userEmail = null;
    userId = null;
    await Utility.setLocalStorage('role', role);
    await Utility.setLocalStorage('userName', null);
    await Utility.setLocalStorage('pass', null);
    await Utility.setLocalStorage('userPin', null);
    await Utility.setLocalStorage('userId', null);
    await Utility.setLocalStorage('userEmail', null);
    if (onUserChange != null) onUserChange();
  }

  static getUserConfig(){
    return '${Constants.configPath}$role/user_config.json';
  }

  static getFormsConfig(){
    return '${Constants.configPath}$role/forms.json';
  }

  static getRegisterConfig(){
    return '${Constants.configPath}register.json';
  }

  static getImageFormsIcon(String idImage){
    return '${Constants.imagesPath}forms_icons/$idImage';
  }

  static getImagePath(String idImage){
    return '${Constants.imagesPath}$idImage';
  }

  static setHastInternetConnection(bool hasInternet){
    hasInternetConnection = hasInternet;
    //if ( hasInternetConnection ) Utility.showToast('Modo online activado');
    //if ( !hasInternetConnection ) Utility.showToast('Modo offline activado');
  }

  static evaluateFirstConnection() async {
    bool hasInternet = await InternetConection.checkInternet();
    User.setHastInternetConnection(hasInternet);
  }
}

import 'package:flutter/material.dart';

import '../widgets/input.dart';
import '../../form/model/option.dart';
import '../../form/model/form_config.dart';
import '../../../util/style.dart';
import '../../../util/utility.dart';

class FormScreen extends StatefulWidget {

  final FormConfig formConfig;

  FormScreen({
    @required this.formConfig,
  });

  @override
  FormScreenState createState() => FormScreenState();
}

class FormScreenState extends State<FormScreen> {

  @override
  void initState() {
    super.initState();
    setState(() {
      widget.formConfig.configOptions(context, _onSave);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode()); // Hide keyboard on dismiss
        },
        child: Scaffold(
            appBar: StyleApp.getAppBar(),
            body: ListView(
              padding: EdgeInsets.all(8.0),
              children: _getInputs(widget.formConfig),
            ),
          ),
    );
  }

  _onSave({idOptionChange = ''}){
    setState(() {
      widget.formConfig.configOptions(context, _onSave);
    });
  }

  List<Widget> _getInputs(FormConfig formConfig){
    List<Widget> w = formConfig.options
        .where( (Option o) => formConfig.isShowOption(o) ).toList()
        .map<Widget>( (Option o) => Input(config: o) ).toList();
    if ( formConfig.submitLabel != null )
    w.add(_getSaveButton(formConfig.submitLabel));
    return w;
  }

  _saveInfo(BuildContext context){
    List<Option> optionsToSave = widget.formConfig.options.where( (Option o) => !o.avoidValue() && widget.formConfig.isShowOption(o) ).toList();

    if ( !_isValidForm(optionsToSave) ) return;

    widget.formConfig.onSave(widget.formConfig);
  }

  _isValidForm(List<Option> optionsToSave){
    List<Option> noValid = optionsToSave.where( (Option o) => !o.valid() ).toList();
    if (noValid.length == 0) return true;
    Utility.showToast('Falta información');
    noValid.forEach((Option o) => o.showValidate = true );
    _onSave(); // SHOW WARNINGS
    return false;
  }

  _getSaveButton(String submitLabel){
    return FlatButton(
      padding: const EdgeInsets.all(0.0),
      onPressed: () => _saveInfo(context),
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        width: 360,
        padding: const EdgeInsets.all(10.0),
        child: Text(
          submitLabel,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
    );
  }

}

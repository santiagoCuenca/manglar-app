import 'dart:convert';

import '../model/option.dart';
import '../../../util/utility.dart';

class FormConfig {

  String idReport;
  String submitLabel;
  String title;

  String image;
  Function onSave;

  List<Option> options;

  FormConfig({
    this.idReport,
    this.submitLabel,
    this.title,
    this.options,
    this.image,
    this.onSave,
  });

  factory FormConfig.fromJson(Map<String, dynamic> parsedJson) {
    List<Option> options = parsedJson['options']
        .map<Option>( (optionApi) => Option.fromJson(optionApi) )
        .toList();
    return FormConfig(
      idReport: parsedJson['idReport'],
      submitLabel: parsedJson['submitLabel'],
      title: parsedJson['iconTitle'],
      image: parsedJson['image'],
      options: options,
    );
  }

  factory FormConfig.fromOptionGroup(Option optionGroup) {
    if ( optionGroup.optionsToGroup.length == 0 ) {
      return FormConfig( title: 'NO DATA FORM GROUP ${optionGroup.id}', options: []);
    }
    return FormConfig(
      idReport: optionGroup.id,
      title: optionGroup.label,
      options: optionGroup.optionsToGroup,
    );
  }

  isShowOption(Option o){
    if ( o.hideOption ) return false;
    if ( o.showIfId == null ) return true;
    Option optionShow = options.firstWhere( (Option option){
      return option.id == o.showIfId;
    });
    if ( optionShow.value == null ) return false; // before if ( optionShow == null ) return true;
    return optionShow.value.state == o.showIfSelected && isShowOption(optionShow);
  }

  addOption(Option o, Function onUpdate){
    int current = _getCurrentExtraOptions(o);
    if ( o.maxExtraOptions != null && current == o.maxExtraOptions ) {
      Utility.showToastMoreTime('Máximo ${o.maxExtraOptions} ${o.label.toLowerCase()}');
      return;
    }
    int indexToInsert = options.indexOf(o) + current + 1;
    List<Option> optionsToAdd = json.decode(o.optionsToAdd)
        .map<Option>( (o) => Option.fromJson(o)).toList();
    optionsToAdd.reversed.forEach( (Option newO){
      newO.createdByUser = true;
      options.insert(indexToInsert, newO);
    });
    onUpdate();
  }

  removeOption(context, Option o, Function onUpdate){
    int indexToRemove = options.indexOf(o) + 1;
    if ( options.length == indexToRemove ||
        !options.asMap()[indexToRemove].createdByUser ) {
      Utility.showToastMoreTime('Lista de opciones vacia');
      return; // Nothing to remove
    }
    int current = _getCurrentExtraOptions(o);
    /*if ( o.minExtraOptions != null && current == o.minExtraOptions ) {
      Utility.showToastMoreTime('Mínimo ${o.minExtraOptions} ${o.label.toLowerCase()}');
      return;
    }*/
    Utility.showConfirm(context, 'Confirmación', '¿Está seguro de eliminar esta opción?', () {
      json.decode(o.optionsToAdd).forEach( (var o) {
        int lastAdded = indexToRemove + current - 1;
        Option oToRemove = options.asMap()[lastAdded];
        if ( oToRemove.createdByUser ) options.removeAt(lastAdded);
      });
      onUpdate();
    });
  }

  configOptions(context, Function onUpdate) {
    _evaluateExtraOptions(onUpdate);
    _addOnClick(context, onUpdate);
  }

  _evaluateExtraOptions(Function onUpdate) {
    bool needsUpdate = false;
    for ( int i = 0; i < options.length; i++ ) {
      Option option = options.asMap()[i];
      if ( option.type == 'addOption' &&
          option.minExtraOptions != null &&
          !option.minExtraOptionsAdded ) {
        int currentOptions = _getCurrentExtraOptions(option);
        int needed = option.minExtraOptions - currentOptions;
        if ( needed > 0 ) {
          for ( int n = 0; n < needed; n++ ) {
            addOption(option, () => {});
          }
          i += needed;
          needsUpdate = true;
          option.minExtraOptionsAdded = true;
        }
      }
    }
    if (needsUpdate) {
      onUpdate();
    }
  }

  _getCurrentExtraOptions(Option option){
    Map<String, dynamic> optionJson = json.decode(option.optionsToAdd).asMap()[0];
    String id = Option.fromJson(optionJson).id;
    return options.where( (Option o) => o.id == id ).toList().length;
  }

  _addOnClick(context, Function onUpdate){
    options.forEach( (Option o) {
      switch (o.type) {
        case 'addOption':
          o.onClick = () => addOption(o, onUpdate);
          o.onClick2 = () => removeOption(context, o, onUpdate);
          break;
        case 'calendar':
        case 'toggle':
          o.onClick = (value) => o.setValue(value, onUpdate);
          break;
        case 'select':
          o.onClick = (value) => o.setValueByIdOption(value, onUpdate);
          break;
        case 'selectMultiple':
          o.onClick = (values) => o.setMultipleValues(values, onUpdate);
          break;
        case 'groupInputs':
        case 'gpsInput':
        case 'file':
          o.onClick = () => onUpdate();
          break;
      }
    });
  }

}

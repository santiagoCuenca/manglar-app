import 'package:flutter/material.dart';
import 'dart:convert';

import './data.dart';

class Option {

  static const NO_VALUE = 'NO_VALUE';

  String id, label, type, placeholder, typeInput, pathImageVideo;
  bool isRequired = false, createdByUser = false, showValidate = false, isEditable = true, hideOption = false;
  List<Data> options;
  Function onClick;
  Function onClick2; // Add options needs to buttons
  String optionsToAdd; // IS A JSON STRING, MOST BE DECODED
  List<Option> optionsToGroup; // IS A JSON STRING, MOST BE DECODED
  String showIfId, showIfSelected;
  Data value; // THE VALUE TO SAVE
  String textHelp;
  Set<int> selectedValues; // Used on multiple selection
  TextEditingController textController = TextEditingController();
  int minExtraOptions, maxExtraOptions;
  bool minExtraOptionsAdded = false;
  String validate; // Used to validate the input

  Option({
    this.id,
    this.label,
    this.type,
    this.isRequired,
    this.placeholder,
    this.typeInput,
    this.pathImageVideo,
    this.textHelp,
    this.value,
    this.options,
    this.optionsToAdd,
    this.optionsToGroup,
    this.showIfId,
    this.showIfSelected,
    this.selectedValues,
    this.minExtraOptions,
    this.maxExtraOptions,
    this.validate,
  });

  // FROM API
  factory Option.fromJson(optionApi){
    List<Data> options = [];
    List<Option> optionsToGroup = [];
    Data value;
    if (optionApi['options'] != null) {
      options = optionApi['options'].map<Data>( (o) => Data(id: o.toString(), state: o.toString()) ).toList();
      value = options[0];
    }
    if (optionApi['optionsToGroup'] != null) {
      optionsToGroup = optionApi['optionsToGroup']
          .map<Option>( (optionApi) => Option.fromJson(optionApi) )
          .toList();
    }
    return Option(
      id: optionApi['id'],
      label: optionApi['label'],
      type: optionApi['type'],
      isRequired: optionApi['isRequired'] != null && optionApi['isRequired'],
      placeholder: optionApi['placeholder'],
      typeInput: optionApi['typeInput'],
      pathImageVideo: optionApi['image'],
      value: value,
      textHelp: optionApi['textHelp'],
      validate: optionApi['validate'],
      showIfId: optionApi['showIfId'],
      showIfSelected: optionApi['showIfSelected'],
      options: options,
      optionsToAdd: json.encode(optionApi['optionsToAdd']),
      optionsToGroup: optionsToGroup,
      selectedValues: [1].toSet(), // By default
      minExtraOptions: optionApi['minExtraOptions'],
      maxExtraOptions: optionApi['maxExtraOptions'],
    );
  }

  String getValue() {
    if ( type == 'selectMultiple' )
      return selectedValues.fold( '', (r, k) =>'$r ${options[k-1]}').trim();
    if ( value != null ) return value.id;
    if ( textController.text != null && textController.text != '' ) return textController.text;
    if ( type == 'toggle' ) return 'false';
    return NO_VALUE;
  }

  dynamic getValueFormatted(){
    if ( !hasValue() ) return null;
    String current = getValue();
    switch (type) {
      case 'toggle':
        return current == 'true';
      case 'input':
        return typeInput == 'number' ? int.tryParse(current) : current;
      default:
        return current;
    }
  }

  bool hasValue(){
   return getValue() != NO_VALUE;
  }

  bool avoidValue(){
    return type == 'title' || type == 'subtitle' || type == 'addOption';
  }

  bool valid(){
    if (!isRequired) return true;
    return getValue() != NO_VALUE;
  }

  setValueByIdOption(String idValue, onUpdate){
    //print('si Error, no encuentra el id $idValue');
    value = options.firstWhere((option) => option.id == idValue);
    onUpdate(idOptionChange: id);
  }

  setValue(String v, onUpdate){
    value = Data(id: v, state: v);
    onUpdate(idOptionChange: id);
  }

  setMultipleValues(values, Function onUpdate){
    selectedValues = values;
    onUpdate(idOptionChange: id);
  }

  setTextToController(String value, Function onUpdate){
    textController.text = value;
    onUpdate(idOptionChange: id);
  }

}

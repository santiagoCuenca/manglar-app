import 'package:flutter/material.dart';

import '../model/form_menu.dart';

import '../../form/model/form_config.dart';
import '../../form/screens/form_screen.dart';

import '../../../util/style.dart';
import '../../../util/utility.dart';
import '../../../util/user.dart';

class FormMenuWidget extends StatelessWidget{

  final FormMenu formMenu;

  FormMenuWidget({
    Key key,
    this.formMenu,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final headerTitle = Container(
      padding: EdgeInsets.fromLTRB(10, 30, 10, 30),
      child: Column(
        children: <Widget>[
          Text(
            getTitle(),
            textAlign: TextAlign.center,
            style: StyleApp.getStyleTitle(22),
          ),
          SizedBox(height: 8.0),
          Text(
            getSubTitle(),
            textAlign: TextAlign.center,
            style: StyleApp.getStyleSubTitle(14),
          ),
        ],
      ),
    );

    final items = Container(
      child: GridView.count(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        crossAxisCount: 3,
        padding: EdgeInsets.all(4),
        children: formMenu.forms.map<Widget>( (FormConfig formConfig) {
          return getItem(context, formConfig);
        }).toList(),
      ),
    );

    return ListView(
      padding: EdgeInsets.all(8.0),
      children: <Widget>[
        headerTitle,
        items,
      ],
    );
  }

  getTitle(){
    return formMenu.title ?? 'no_title';
  }

  getSubTitle(){
    return formMenu.subTitle ?? '';
  }

  Widget getItem(BuildContext context, FormConfig formConfig){
    String imageAsset = User.getImageFormsIcon(formConfig.image);
    String titleToUse = formConfig.title ?? 'no_title';
    return Container(
      child: FlatButton(
        onPressed: () {
          Utility.navTo(context, FormScreen(formConfig: formConfig,));
        },
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              width: 120,
              height: 60,
              child: Image.asset(imageAsset),
            ),
            Text(
              titleToUse,
              textAlign: TextAlign.center,
              style: StyleApp.getStyleSubTitle(13),
            ),
          ],
        )),
    );
  }

}

import 'dart:io';
import 'dart:convert';

import '../model/anomaly_form.dart';

import '../../../constants.dart';
import '../../../util/user.dart';
import '../../../plugins/upload.dart';
import '../../../util/utility.dart';
import '../model/anomaly_evidence.dart';
import '../controller/offline_controller.dart';
import '../repository/anomaly_form_repository.dart';

class AnomalySaveController {

  final _anomalyFormRepository = AnomalyFormRepository();
  final _offlineController = OfflineController();

  Future<bool> save(context, anomalyForm) async {
    bool hasInternetConnection = User.hasInternetConnection;
    if ( hasInternetConnection ) {
      bool success = await _saveOnline(context, anomalyForm);
      return success;
    }
    return await _saveOffline(context, anomalyForm);
  }

  Future<bool> saveFromLocalStorage(context) async {
    List<String> formsIds = await _offlineController.getFormsOnLocal();
    if ( formsIds.length == 0 ) return true;

    bool success = true;
    for ( String formId in formsIds ) {
      String formData = await Utility.getLocalStorage(formId);
      if (formData == 'null' ) {
        await _offlineController.removeFormsOnLocal(formId);
        return success;
      }
      final _anomalyForm = AnomalyForm.fromJson(json.decode(formData));
      bool successSaveForm = await _saveOnline(context, _anomalyForm);
      if ( !successSaveForm ) success = false;
      // if ( successSaveForm ) // TODO EVALUATE IF IS NECESSARY TRY AGAIN ON LOCAL STORAGE
        await _offlineController.removeFormsOnLocal(formId);
    }
    return success;
  }

  Future<bool> _saveOffline(context, anomalyForm) async {
    String msg = 'Se guardará cuando tenga internet';
    bool success = true;
    Utility.showLoading(context);
    try {
      await _offlineController.saveToLocalStorage(anomalyForm);
    } catch (e) {
      print('Error $e');
      msg = 'Error no se pudo guardar en local';
      success = false;
    }
    Utility.dismissLoading(context);
    Utility.showToast(msg);
    return success;
  }

  Future<bool> _saveOnline(context, AnomalyForm anomalyForm) async {

    String data = json.encode(anomalyForm.toJson());
    String msg = 'Formulario guardado correctamente';

    Utility.showLoading(context, msg: 'Guardando formulario por favor no cerrar la app');

    bool successUploadEvidences = await _uploadEvidences(anomalyForm);
    if (!successUploadEvidences) {
      Utility.dismissLoading(context);
      return false;
    }
    bool successSaveForm = (await _anomalyFormRepository.save(data)).state == 'OK';
    if (!successSaveForm) {
      msg = 'Error al guardar formulario';
    }

    Utility.dismissLoading(context);
    Utility.showToast(msg);

    return successUploadEvidences && successSaveForm;
  }

  Future<bool> _uploadEvidences(AnomalyForm anomalyForm) async {
    List<AnomalyEvidence> evidencesNotEmpty = anomalyForm.anomaliesEvidences
        .where( (AnomalyEvidence evidence) => !evidence.isEmpty() ).toList();
    if (evidencesNotEmpty.length == 0) return true;

    bool success = true;
    for (AnomalyEvidence evidence in evidencesNotEmpty) {
      String uploadURL = '${Constants.hostUrl}rest/evidence/upload';
      String nameFile = evidence.anomalyEvidenceNameFile;
      String imagePath = evidence.anomalyEvidencePathOrigin;
      try {
        File imageFile = File(imagePath);
        String stateUpload = await Upload.uploadFile(imageFile, uploadURL, nameFile);
        if (stateUpload != 'OK') {
          success = false;
          Utility.showToastMoreTime('ERROR foto/video $nameFile s:$stateUpload');
        }
      } catch (e) {
        success = false;
        Utility.showToastMoreTime('ERROR foto/video $nameFile $e');
      }
    }
    return success;
  }

}

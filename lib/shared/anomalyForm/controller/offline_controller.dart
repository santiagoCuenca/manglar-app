import 'dart:convert';

import '../../../util/utility.dart';

import '../model/anomaly_form.dart';

class OfflineController {

  static const String FORMS_LOCAL = 'FORMS_LOCAL';

  Future<List<String>> getFormsOnLocal() async {
    String forms = await Utility.getLocalStorage(FORMS_LOCAL);
    if (forms == null) return [];
    return json.decode(forms).map<String>( (f) => f.toString() ).toList();
  }

  addFormsOnLocal(String formId) async {
    List<String> current = await getFormsOnLocal();
    current.add(formId);
    await Utility.setLocalStorage(FORMS_LOCAL, json.encode(current));
  }

  removeFormsOnLocal(String formId) async {
    List<String> current = await getFormsOnLocal();
    current.remove(formId);
    await Utility.setLocalStorage(FORMS_LOCAL, json.encode(current));
  }

  Future<String> saveToLocalStorage(AnomalyForm anomalyForm) async {
    String formId = (new DateTime.now().millisecondsSinceEpoch).toString();
    String data = json.encode(anomalyForm.toJson());
    await Utility.setLocalStorage(formId, data);
    await addFormsOnLocal(formId);
    return formId;
  }

  Future<AnomalyForm> getFromLocalStorage(String formId) async {
    String data = await Utility.getLocalStorage(formId);
    return AnomalyForm.fromJson(json.decode(data));
  }

}
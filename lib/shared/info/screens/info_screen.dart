import 'package:flutter/material.dart';
import '../../../util/style.dart';
import '../widgets/info.dart';

class InfoScreen extends StatelessWidget {

  final String title;
  final Widget info;

  InfoScreen({this.title, this.info});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleApp.getAppBar(),
      body: Info(title: title, info: info,),
    );
  }

}

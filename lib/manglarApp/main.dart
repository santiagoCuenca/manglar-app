import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'navigation.dart';
import '../shared/intro/screens/intro.dart';

import '../util/user.dart';
import '../util/utility.dart';
import '../plugins/internet_connection.dart';
import '../constants.dart';
import '../shared/anomalyForm/controller/anomaly_save_controller.dart';

class ManglarApp extends StatefulWidget{
  @override
  _State createState() {
    return _State();
  }
}

class _State extends State<ManglarApp> {

  bool _loaded = false;
  bool _isLogged = false;
  StreamSubscription subscription;
  final _anomalySaveController = AnomalySaveController();

  @override
  void initState() {
    super.initState();
    User.evaluateFirstConnection();
    subscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      bool isConnected = result == ConnectivityResult.mobile || result == ConnectivityResult.wifi;
      if ( isConnected ) _saveLocalStorageForms();
      if ( !isConnected ) User.setHastInternetConnection(false);
    });
    User.setOnUserChange(_loadUserData);
    User.setInfoFromLocalStorage().then((_) {
      _loadUserData();
    });
  }

  @override
  dispose() {
    super.dispose();
    subscription.cancel();
  }

  _saveLocalStorageForms() async {
    bool hasInternet = await InternetConection.checkInternet();
    User.setHastInternetConnection(hasInternet);
    if ( hasInternet ) {
      await _anomalySaveController.saveFromLocalStorage(context);
    }
  }

  _loadUserData() {
    String pathUsers = User.getUserConfig();
    bool isLogged = User.role != "public";
    if (isLogged) {
      Utility.showToast('Bienvenido ${User.userName}');
      Utility.loadConfig(pathUsers).then((configUser) =>
        setState(() {
          _isLogged = isLogged;
          _loaded = true;
        }));
    }
    else {
      setState(() {
        _isLogged = isLogged;
        _loaded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return _loaded
        ? _materialApp()
        : new Center(child: new CircularProgressIndicator(),);
  }

  _materialApp() {
    return MaterialApp(
      title: 'ManglarApp',
      theme: ThemeData(
        primaryColor: Color(Constants.colorPrimary),
        primaryColorLight: Color(Constants.colorSecondary),
        // Define the default font family.
        fontFamily: 'Bookman',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: _isLogged ? Navigation() : Intro(),
    );
  }

}
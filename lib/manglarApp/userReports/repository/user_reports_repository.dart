
import '../../../shared/anomalyForm/model/anomaly_form.dart';

import './user_reports_api.dart';

class UserReportsRepository {

  final _userReportsApi = UserReportsApi();

  Future<List<AnomalyForm>> getByUserId(int userId) => _userReportsApi.getByUserId(userId);

}

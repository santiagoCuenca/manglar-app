import '../../../shared/anomalyForm/model/anomaly_form.dart';
import '../../../util/web_client.dart';

class UserReportsApi {

  final WebClient _webClient = new WebClient();

  Future<List<AnomalyForm>> getByUserId(int userId) async {
    dynamic result = await _webClient.get('rest/anomaly-form-reporter/get/$userId');
    return result.map<AnomalyForm>( (o) => AnomalyForm.fromJson(o) ).toList();
  }

}

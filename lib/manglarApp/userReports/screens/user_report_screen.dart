
import '../../../shared/anomalyForm/model/anomaly_form.dart';

import '../../../util/style.dart';
import '../../../util/user.dart';

import 'package:flutter/material.dart';
import '../widgets/user_report.dart';
import '../repository/user_reports_repository.dart';

class UserReportScreen extends StatefulWidget {
  @override
  _State createState() {
    return _State();
  }
}

class _State extends State<UserReportScreen>{

  final _userReportsRepository = UserReportsRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleApp.getAppBar(),
      body: FutureBuilder(
        future: _userReportsRepository.getByUserId(int.tryParse(User.userId)),
        builder: (context, snapshot){
          if (snapshot.hasData) {
            return _list(snapshot.data);
          } else if (snapshot.hasError) {
            return _listError(snapshot.error.toString());
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  _list(List<AnomalyForm> anomaliesForms){

    List<Widget> children = [StyleApp.getTitle('REPORTES')];
    List<Widget> reports = anomaliesForms.map<Widget>( (AnomalyForm a) {
      return UserReport(
        type: a.anomalyFormType,
        typeId: a.anomalyFormTypeId,
        date: a.createdAt.toString(),
        desc: a.description,
        state: a.anomalyFormState,
      );
    }).toList();
    if (reports.length > 0) {
      children.addAll(reports);
    }
    else {
      children.add(
        Text(
          'Lista de reportes vacia',
          textAlign: TextAlign.center,style: StyleApp.getStyleSubTitle(15),
        )
      );
    }

    return ListView(
      children: children,
    );
  }

  _listError(error){
    List<Widget> children = [
      StyleApp.getTitle('REPORTES'),
      Text(
        'No se pudo obtener la lista de reportes',
        textAlign: TextAlign.center,style: StyleApp.getStyleSubTitle(15),
      ),
    ];
    return ListView(
      children: children,
    );
  }

}

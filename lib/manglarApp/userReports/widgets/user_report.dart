import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../util/style.dart';
import '../../../util/user.dart';

class UserReport extends StatelessWidget {

  final String type, typeId, state, desc, date;
  final _formatDate = new DateFormat('yyyy-MM-dd hh:mm');

  UserReport({this.type, this.typeId, this.state, this.desc, this.date});

  @override
  Widget build(BuildContext context) {

    final image = Container(
      padding: EdgeInsets.all(20),
      width: 100,
      height: 100,
      child: _getImage(),
    );

    final double cWidth = MediaQuery.of(context).size.width*0.65;
    String formatDate = _formatDate.format(
        DateTime.fromMillisecondsSinceEpoch(int.tryParse(date) ?? 0));

    final info = Container(
      padding: EdgeInsets.all(5),
      width: cWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Tipo: $type', style: StyleApp.getStyleSubTitle(16),),
          _getText('Estado: $state', style: StyleApp.getStyleSubTitle(14),),
          _getText('Fecha: $formatDate'),
          _getText('Descripción: $desc'),
        ],
      ),
    );

    return Container(
      margin: EdgeInsets.all(12),
      decoration: StyleApp.getBoxDecoration(),
      child: Row(
        children: <Widget>[
          image,
          info,
        ],
      ),
    );
  }

  _getText(String text, {style}){
    return Text(text, overflow: TextOverflow.ellipsis, maxLines: 2, style: style);
  }

  _getImage(){
    String imageFile = '$typeId.png';
    return Image.asset('${User.getImageFormsIcon(imageFile)}');
  }

}

import 'package:flutter/material.dart';

import '../repository/home_repository.dart';

import '../../../shared/anomalyForm/controller/anomaly_save_controller.dart';
import '../../../shared/anomalyForm/model/anomaly_evidence.dart';
import '../../../shared/anomalyForm/model/anomaly_offender.dart';
import '../../../shared/anomalyForm/model/anomaly_witness.dart';
import '../../../shared/formMenu/model/form_menu.dart';

import '../../../constants.dart';
import '../../../shared/formMenu/widgets/form_menu_widget.dart';
import '../../../util/style.dart';
import '../../../util/user.dart';
import '../../../util/utility.dart';
import '../../../shared/form/model/form_config.dart';
import '../../../shared/form/model/option.dart';
import '../../../shared/anomalyForm/model/anomaly_form.dart';
import '../../../shared/form/screens/success_form.dart';

class Home extends StatefulWidget{
  @override
  _State createState() {
    return _State();
  }
}

class _State extends State<Home>{

  final _anomalySaveController = AnomalySaveController();
  final _homeRepository = HomeRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleApp.getAppBar(),
      body: FutureBuilder(
        future: _homeRepository.getFormMenu(),
        builder: (context, snapshot){
          if (snapshot.hasData) {
            return FormMenuWidget(formMenu: _setOnSaveActions(snapshot.data),);
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return new Center(child: new CircularProgressIndicator(),);
        },
      ),
    );
  }

  _setOnSaveActions(FormMenu formMenu){
    formMenu.forms.forEach( (FormConfig formConfig){
      formConfig.onSave = _save;
    });
    return formMenu;
  }

  _save(FormConfig formConfig) async {
    AnomalyForm anomalyForm = _getForm(formConfig);
    bool success = await _anomalySaveController.save(context, anomalyForm);
    if (success){
      Navigator.pop(context);
      Utility.navTo(context, SuccessForm(idReport: formConfig.idReport,));
    }
  }

  AnomalyForm _getForm(FormConfig formConfig){
    int userId = int.tryParse(User.userId);
    String anomalyFormState = 'Informe técnico';
    List<Option> options = formConfig.options
        .where( (Option o) => formConfig.isShowOption(o) ).toList();
    return AnomalyForm(
      userId: userId,
      anomalyFormType: formConfig.idReport,
      anomalyFormStatus: true,
      anomalyFormState: anomalyFormState,
      area: _getOptionValue(options, 'area'),
      industrialType: _getOptionValue(options, 'industrial_type'),
      artesanalType: _getOptionValue(options, 'artesanal_type'),
      fishingType: _getOptionValue(options, 'fishing_type'),
      fishingTypeOthers: _getOptionValue(options, 'fishing_type_others'),
      date: _getOptionValue(options, 'date'),
      beachingReason: _getOptionValue(options, 'beaching_reason'),
      animalsStrandedCount: _getOptionValue(options, 'animals_stranded_count'),
      animalsStrandedSize: _getOptionValue(options, 'animals_stranded_size'),
      seaConditions: _getOptionValue(options, 'sea_conditions'),
      anomalyFormSubtype: _getOptionValue(options, 'anomaly_form_subtype'),
      type: _getOptionValue(options, 'type'),
      custody: _getOptionValue(options, 'custody'),
      protectedArea: _getOptionValue(options, 'protected_area'),
      description: _getOptionValue(options, 'description'),
      individualsRequisitionedInfo: _getOptionValue(options, 'individuals_requisitioned_info'),
      individuals: _getOptionValue(options, 'individuals'),
      individualsRequisitioned: _getOptionValue(options, 'individuals_requisitioned'),
      province: _getOptionValue(options, 'province').toUpperCase(),
      canton: _getOptionValue(options, 'canton').toUpperCase(),
      location: _getOptionValue(options, 'location'),
      latlong: _getOptionValue(options, 'latlong'),
      address: _getOptionValue(options, 'address'),
      estuary: _getOptionValue(options, 'estuary'),
      community: _getOptionValue(options, 'community'),
      feelDanger: _getOptionValue(options, 'feel_danger'),
      hideIdentity: _getOptionValue(options, 'hide_identity'),
      anomaliesOffenders: _getOffenders(options),
      anomaliesWitnesses: _getWitnesses(options),
      anomaliesEvidences: _getEvidences(options),
    );
  }

  dynamic _getOptionValue(List<Option> options, String idOption){
    Option option = _getOption(options, idOption);
    if ( option != null ) {
      return option.getValueFormatted();
    }
    return null;
  }

  Option _getOption(List<Option> options, String idOption){
    return options
        .firstWhere( (Option o) => o.id == idOption, orElse: () => null );
  }

  dynamic _getOptionValueValidateIf(List<Option> options, String idOption){
    Option option = options
        .firstWhere( (Option o) => o.id == idOption && _isShowOption(options, o), orElse: () => null );
    if ( option != null ) {
      return option.getValueFormatted();
    }
    return null;
  }

  _isShowOption(List<Option> options, Option o){
    if ( o.hideOption ) return false;
    if ( o.showIfId == null ) return true;
    Option optionShow = options.firstWhere( (Option option){
      return option.id == o.showIfId;
    });
    if ( optionShow.value == null ) return false; // before if ( optionShow == null ) return true;
    return optionShow.value.state == o.showIfSelected && _isShowOption(options, optionShow);
  }

  List<AnomalyOffender> _getOffenders(List<Option> options){
    String idOption = 'offender_info';
    List<Option> optionsToAdd = options.where( (Option o) => o.id == idOption ).toList();
    if ( optionsToAdd.length > 0 ) {
      return optionsToAdd.map( (Option o) {
        List<Option> optionsInfo = o.optionsToGroup;
        return AnomalyOffender(
          anomalyOffenderStatus: true,
          anomalyOffenderName: _getOptionValue(optionsInfo, 'offender_name'),
          anomalyOffenderPin: _getOptionValueValidateIf(optionsInfo, 'offender_pin'),
          anomalyOffenderPhone: _getOptionValue(optionsInfo, 'offender_phone'),
          anomalyOffenderAddress: _getOptionValue(optionsInfo, 'offender_address'),
          anomalyOffenderAdditionalInformation: _getOptionValue(optionsInfo, 'offender_additional_information'),
        );
      }).toList();
    }
    return [];
  }

  List<AnomalyEvidence> _getEvidences(List<Option> options){
    String idOption = 'evidence_info';
    List<Option> optionsToAdd = options.where( (Option o) => o.id == idOption ).toList();
    if ( optionsToAdd.length > 0 ) {
      return optionsToAdd.map( (Option o) {
        List<Option> optionsInfo = o.optionsToGroup;
        Option fileInfo = _getOption(optionsInfo, 'evidence_file_info');
        String nameFile = Utility.fixName(_getOptionValue(optionsInfo, 'evidence_file_info'));
        String url = fileInfo.typeInput == 'photo' ? '${Constants.hostUrl}rest/evidence/images/$nameFile' :
        '${Constants.hostUrl}rest/evidence/videos/$nameFile';
        return AnomalyEvidence(
          anomalyEvidenceStatus: true,
          anomalyEvidenceNameFile: nameFile,
          anomalyEvidenceType: fileInfo.typeInput,
          anomalyEvidencePathOrigin: fileInfo.pathImageVideo,
          anomalyEvidenceUrl: url,
          anomalyEvidenceDescription: _getOptionValue(optionsInfo, 'evidence_description'),
        );
      }).toList();
    }
    return [];
  }

  List<AnomalyWitness> _getWitnesses(List<Option> options){
    String idOption = 'witness_info';
    List<Option> optionsToAdd = options.where( (Option o) => o.id == idOption ).toList();
    if ( optionsToAdd.length > 0 ) {
      return optionsToAdd.map( (Option o) {
        List<Option> optionsInfo = o.optionsToGroup;
        return AnomalyWitness(
          anomalyWitnessStatus: true,
          anomalyWitnessName: _getOptionValue(optionsInfo, 'witness_name'),
          anomalyWitnessPhone: _getOptionValue(optionsInfo, 'witness_phone'),
        );
      }).toList();
    }
    return [];
  }

}

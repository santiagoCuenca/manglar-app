import 'home_api.dart';
import '../../../shared/formMenu/model/form_menu.dart';

class HomeRepository {

  final _homeApi = HomeApi();

  Future<FormMenu> getFormMenu() => _homeApi.getFormMenu();

}

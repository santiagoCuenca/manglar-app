import '../../../shared/form/model/form_config.dart';
import '../../../util/user.dart';
import '../../../util/utility.dart';

import '../../../shared/formMenu/model/form_menu.dart';

class HomeApi {

  Future<FormMenu> getFormMenu() async {
    String pathUsers = User.getUserConfig();
    String pathForms = User.getFormsConfig();
    Map<String, dynamic> configUser = await Utility.loadConfig(pathUsers);
    Map<String, dynamic> configForms = await Utility.loadConfig(pathForms);
    List<FormConfig> forms = (configUser['forms'] ?? []).map<FormConfig>( (formId) {
      Map<String, dynamic> config = configForms[formId];
      return FormConfig.fromJson(config);
    }).toList();
    FormMenu formMenu = FormMenu(
      id: configUser['id'] ?? 'no_id',
      title: configUser['title'] ?? 'no_title',
      subTitle: configUser['subTitle'] ?? 'no_sub_title',
      type: configUser['type'] ?? 'no_type',
      forms: forms,
    );
    return formMenu;
  }

}

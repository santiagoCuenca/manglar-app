import 'package:flutter/material.dart';
import 'home/screens/home.dart';
import 'options.dart';
import '../constants.dart';
import './userReports/screens/user_report_screen.dart';

class Navigation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Navigation();
  }
}

class _Navigation extends State<Navigation> {

  int indexTap = 0;

  final List<Widget> widgetsChildren = [
    Home(),
    UserReportScreen(),
    Options()
  ];

  void onTapTapped(int index){
    setState(() {
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetsChildren[indexTap],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.white,
          primaryColor: Color(Constants.colorPrimary),
        ),
        child: BottomNavigationBar(
          onTap: onTapTapped,
          currentIndex: indexTap,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text("Menú")
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.format_list_bulleted),
                title: Text("Reportes")
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text("Opciones")
            ),
          ]
        ),
      ),
    );
  }

}
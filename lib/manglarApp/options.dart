import 'package:flutter/material.dart';

import '../constants.dart';
import '../shared/info/screens/info_screen.dart';
import '../util/user.dart';
import '../util/utility.dart';
import '../util/style.dart';
import '../shared/profile/screens/profile.dart';

class Options extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final profile = Profile();

    final importanciaManglar = InfoScreen(
        title: 'IMPORTANCIA DEL MANGLAR',
        info: Text('El manglar es un ecosistema boscoso que se desarrolla en la transición entre tierra firme y mar abierto, en las zonas tropicales y subtropicales del mundo. Por sus características, es el hábitat de una gran variedad de fauna como aves, peces, moluscos y crustáceos, dentro del cual se encuentran especies de gran valor para la seguridad alimentaria y que son aprovechadas comercialmente como la concha, cangrejo, robalo, lisa, entre otros. Los manglares brindan a su vez servicios ecosistémicos de protección contra la erosión de las costas, ya que atrapan sedimento y hojarasca entre sus raíces ayudando a rellenar y recobrar terreno; protegen al continente de los huracanes, marejadas, tormentas; atenúan los impactos del Fenómeno de El Niño y también protegen las tierras agrícolas de la salinidad del mar, actuando como filtro y manteniendo la calidad del agua.')
    );

    final acuerdos = InfoScreen(
        title: 'ACUERDOS DE USO SOSTENIBLE Y CUSTODIA DEL ECOSISTEMA MANGLAR',
        info: Text('El marco legal ecuatoriano contempla una herramienta de gestión del ecosistema manglar llamada Acuerdos de Uso Sustentable y Custodia del Manglar, bajo el cual el MAE entrega en custodia los bosques del manglar a usuarios tradicionales asentados a lo largo del perfil costanero. Es un instrumento jurídico que garantiza a los custodios el acceso exclusivo a las áreas de manglar con el derecho de aprovechar sustentablemente los recursos bioacuáticos, pero a su vez tienen la obligación de custodiar el manglar y denunciar cualquier daño ambiental a la autoridad ambiental.')
    );

    final whenCall911 = InfoScreen(
        title: '¿CUANDO SE DEBE USAR EL NÚMERO 911?',
        info: Text('Cuando usted detecta una anomalía por ejemplo de tala o contaminación, que se está ejecutando o realizando en el momento actual, se lo llama una infracción o delito en flagrancia. Si detecta una anomalía en flagrancia debe llamar inmediatamente al ECU 911 por ser una emergencia que perturba el funcionamiento de la sociedad con afectación a las personas, la salud, los bienes o al medio ambiente. Una anomalía puede ser generada por eventos naturales o propios de la actividad humana, ya sea de forma repentina o como resultado de un proceso.')
    );

    final mae = Container(
      height: 220,
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              height: 130,
              child: Image(image: AssetImage(User.getImagePath('logo_MAE.png'))) ),
          Text('Tiene competencia en atención de anomalías de tala, contaminación, tráfico de vida silvestre y pesca de especies amenazadas, hallazgos de vida silvestre, invasiones y extracción no autorizada de recursos.'),
        ],
      ),
    );
    final mpceip = Container(
      height: 240,
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              height: 150,
              child: Image(image: AssetImage(User.getImagePath('logo_MPCEIP.png'))) ),
          Text('Tiene competencia en atención de anomalías de incumplimiento de tallas mínimas y recolección en época de veda.'),
        ],
      ),
    );
    final fiscalia = Container(
      height: 220,
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              height: 130,
              child: Image(image: AssetImage(User.getImagePath('logo_fiscalia.png'))) ),
          Text('Tiene competencia en atención de anomalías de tala, contaminación, tráfico de vida silvestre y pesca de especies amenazadas, invasiones y extracción no autorizada de recursos y delincuencia marítima.'),
        ],
      ),
    );
    final orgs = InfoScreen(
        title: 'INSTITUCIONES INVOLUCRADAS',
        info: Container(
          padding: EdgeInsets.only(top: 30),
          child: Column(
            children: <Widget>[
              mae,
              mpceip,
              fiscalia,
            ],
          ),
        ),
    );

    final devs = Container(
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              height: 160,
              child: Image(image: AssetImage(User.getImagePath('logo_MAE.png'))) ),
          Container(
              padding: EdgeInsets.all(10),
              height: 150,
              child: Image(image: AssetImage(User.getImagePath('logo_UTPL.png'))) ),
          Container(
              padding: EdgeInsets.all(10),
              height: 130,
              child: Image(image: AssetImage(User.getImagePath('logo_GIZ.png'))) ),
          Text('El Ministerio del Ambiente, con apoyo de la Universidad Técnica Particular de Loja (UTPL) y la Cooperación Alemana al Desarrollo - GIZ diseñaron esta herramienta de gobierno electrónico llamado ManglarApp, una aplicación para teléfonos móviles iOS y Android que permitirá la denuncia, notificación y monitoreo de problemas socio-ambientales en los manglares custodiados por organizaciones de usuarios tradicionales, los esteros y mar aledaños a estas áreas.'),
        ],
      ),
    );
    final responsibles = InfoScreen(
        title: 'RESPONSABLES DEL DESARROLLO',
        info: devs,
    );

    final logOut = InkWell(
      onTap: (){
        User.logout();
      },
      child: ListTile(
        leading: _getIcon('salir'),
        dense: true,
        title: Text('Salir', textAlign: TextAlign.left, style: StyleApp.getStyleSubTitle(16)),
      ),
    );

    final version = Container(
      padding: EdgeInsets.all(10),
      child: Text(Constants.version, textAlign: TextAlign.right, style: StyleApp.getStyleTitle(10)),
    );

    return Scaffold(
      appBar: StyleApp.getAppBar(),
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
        child: ListView(
          children: <Widget>[
            StyleApp.getTitle('OPCIONES'),
            _option(context, 'Perfil', 'profile', profile),
            _option(context, 'Importancia del Manglar', 'importancia', importanciaManglar),
            _option(context, '¿Cuando llamar al 911?', '911', whenCall911),
            _option(context, 'Acuerdos de uso sustentable', 'acuerdos', acuerdos),
            _option(context, 'Instituciones involucradas', 'orgs', orgs),
            _option(context, 'Responsables del desarrollo', 'responsables', responsibles),
            logOut,
            version,
          ],
        )
      ),
    );
  }

  _option(BuildContext context, String title, String image, Widget w){
    return InkWell(
      onTap: (){
        Utility.navTo(context, w);
      },
      child: ListTile(
        leading: _getIcon(image),
        dense: true,
        title: Text(title, textAlign: TextAlign.left, style: StyleApp.getStyleSubTitle(16)),
      ),
    );
  }

  _getIcon(String image){
    switch (image) {
      case 'profile':
        return Icon(Icons.account_circle);
      case 'importancia':
        return Icon(Icons.info_outline);
      case '911':
        return Icon(Icons.dialer_sip);
      case 'acuerdos':
        return Icon(Icons.pageview);
      case 'orgs':
        return Icon(Icons.business);
      case 'responsables':
        return Icon(Icons.burst_mode);
      case 'salir':
        return Icon(Icons.close);
    }
  }

}
import 'package:file_picker/file_picker.dart';

class FilePickerApp {

  static Future<String> getImagePath() async {
    String filePath = await FilePicker.getFilePath(type: FileType.ANY);
    String nameFile = filePath != null
        ? filePath.split('/').last : 'no-name';
    if (nameFile.contains('.mp4') || nameFile.contains('.jpg') || nameFile.contains('.png')) {
      return filePath;
    }
    return null;
  }

}

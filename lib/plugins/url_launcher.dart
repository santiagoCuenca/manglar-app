import 'package:url_launcher/url_launcher.dart';

class UrlLauncher {

  // url => 'https://flutter.dev'
  static launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // number +593999123757
  static callNumber(String number){
    String url = 'tel:$number';
    launchURL(url);
  }

  // phone with 593 instead 0 example: 593999123757
  static launchWS(String phone, String text) async {
    String url = 'https://api.whatsapp.com/send?phone=$phone&text=$text';
    await launchURL(url);
  }

}
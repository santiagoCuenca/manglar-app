
class Constants {

  static String version = "v1.0.3";

  //static String hostUrl = "http://10.0.2.2:8080/manglar-app/"; // LOCAL
  //static String hostUrl = "http://52.204.108.31/manglar-app/"; // PROD
  static String hostUrl = 'http://qa-suiaint.ambiente.gob.ec/manglar-app/'; // SUIA TEST

  // MANGLARAPP
  static int colorPrimary = 0xff4C990B;
  static int colorSecondary = 0xff1E6561;
  static int colorDark = 0xff232222;
  static int colorTertiary = 0xFF33B878;
  static bool simpleHeader = false;
  static String configPath = "assets/configManglarApp/";
  static String imagesPath = "assets/imagesManglarApp/";

}
